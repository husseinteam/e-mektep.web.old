(function() {
  var assert, loginApi;

  loginApi = require("../build/api/membership/login.api.compiled");

  assert = require("assert");

  describe("loginApi", function() {
    return it("should connect to websocket", function(done) {
      var cb;
      this.timeout(5000);
      cb = {
        onConnection: function(conn) {
          assert(conn != null, "conn not null");
          return conn.sendUTF(JSON.stringify({
            logon: "25871077074",
            password: "1q2w3e4r5t"
          }));
        },
        onMessage: function(message) {
          var token, uid;
          done = message.done, token = message.token, uid = message.uid;
          assert(done("authentication failed"));
          assert(typeof token === "function" ? token("token is " + token) : void 0);
          assert(typeof uid === "function" ? uid("uid is " + uid) : void 0);
          return done();
        },
        failback: function(error) {
          console.log("error is " + error);
          return done();
        }
      };
      return done();
    });
  });

}).call(this);
