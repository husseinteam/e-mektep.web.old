(function() {
  var Client, config;

  config = require(__dirname + "/config.compiled");

  Client = (require("websocket")).client;

  module.exports = function(parameters, onConnection, onMessage, failback) {
    var client, path;
    path = config.ApiHost + "/ws/" + parameters.route;
    client = new Client;
    client.on("connectFailed", function(error) {
      return typeof failback === "function" ? failback("connection failed: " + error) : void 0;
    });
    client.on("connect", function(connection) {
      console.log("WebSocket Client Connected");
      connection.on("error", function(error) {
        return typeof failback === "function" ? failback("connection error: " + error) : void 0;
      });
      connection.on("close", function() {
        return console.log("connection Closed");
      });
      connection.on("message", function(message) {
        return onMessage(message);
      });
      return onConnection(connection);
    });
    return client.connect(path, "");
  };

}).call(this);
