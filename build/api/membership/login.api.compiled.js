(function() {
  var api;

  api = require("../base/api.compiled");

  module.exports = function(cb) {
    var args, parameters;
    args = {
      headers: {
        'test-header': 'client-api'
      }
    };
    parameters = {
      args: args,
      route: 'ms/login'
    };
    return api(parameters, cb.onConnection, cb.onMessage, cb.failback);
  };

}).call(this);
