(function() {
  var glob, options;

  glob = require("glob");

  options = {
    nodir: true,
    cwd: "" + __dirname
  };

  module.exports = function(app) {
    var file, files, i, len, results, route, routes;
    files = glob.sync("**/*.route.compiled.js", options);
    routes = (function() {
      var i, len, results;
      results = [];
      for (i = 0, len = files.length; i < len; i++) {
        file = files[i];
        results.push(require(__dirname + "/" + file));
      }
      return results;
    })();
    results = [];
    for (i = 0, len = routes.length; i < len; i++) {
      route = routes[i];
      results.push((function(route) {
        return route(app);
      })(route));
    }
    return results;
  };

}).call(this);
