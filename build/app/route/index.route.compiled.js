(function() {
  var locals;

  locals = require("../locals/layout.locals.compiled");

  module.exports = function(app) {
    return app.get('/', function(req, res) {
      locals(app);
      return res.render('index');
    });
  };

}).call(this);
