(function() {
  var locals;

  locals = require("../../locals/membership/register.locals.compiled");

  module.exports = function(app) {
    return app.get('/uyelik/yenikayit/:token?', function(req, res, next) {
      locals(app);
      return res.status(200).render('membership/register');
    });
  };

}).call(this);
