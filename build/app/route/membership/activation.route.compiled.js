(function() {
  var locals;

  locals = require("../../locals/membership/activation.locals.compiled");

  module.exports = function(app) {
    return app.get('/uyelik/aktivasyon/:token?', function(req, res, next) {
      locals(app);
      return res.status(200).render('membership/activation');
    });
  };

}).call(this);
