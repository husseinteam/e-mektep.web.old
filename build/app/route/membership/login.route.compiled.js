(function() {
  var locals, loginapi;

  loginapi = require("../../../api/membership/login.api.compiled");

  locals = require("../../locals/membership/login.locals.compiled");

  module.exports = function(app) {
    app.get('/uyelik/oturumac', function(req, res, next) {
      locals(app);
      return res.status(200).render('membership/login');
    });
    return app.post('/uyelik/oturumac/post', function(req, res) {
      var cb;
      cb = {
        onConnection: function(conn) {
          return conn.sendUTF(JSON.stringify({
            logon: req.param('logon', "25871077074"),
            password: req.param('password', "25871077074")
          }));
        },
        onMessage: function(message) {
          var done, token, uid;
          done = message.done, token = message.token, uid = message.uid;
          return res.status(200).render('membership/login', message);
        },
        failback: function(error) {
          return console.log("error is " + error);
        }
      };
      return loginapi(cb);
    });
  };

}).call(this);
