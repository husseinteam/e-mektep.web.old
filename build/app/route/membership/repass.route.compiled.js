(function() {
  var locals;

  locals = require("../../locals/membership/repass.locals.compiled");

  module.exports = function(app) {
    return app.get('/uyelik/sifremiunuttum/:token?', function(req, res, next) {
      locals(app);
      return res.status(200).render('membership/repass');
    });
  };

}).call(this);
