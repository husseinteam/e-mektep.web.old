(function() {
  var locals;

  locals = require("../../locals/schooling/lectures.locals.compiled");

  module.exports = function(app) {
    return app.get('/tedrisat/dersler', function(req, res, next) {
      locals(app);
      return res.status(200).render('schooling/lectures');
    });
  };

}).call(this);
