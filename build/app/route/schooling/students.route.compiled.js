(function() {
  var locals;

  locals = require("../../locals/schooling/students.locals.compiled");

  module.exports = function(app) {
    return app.get('/tedrisat/ogrenciler/:id', function(req, res, next) {
      locals(app);
      return res.status(200).render('schooling/students');
    });
  };

}).call(this);
