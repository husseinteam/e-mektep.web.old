(function() {
  var locals;

  locals = require("../../locals/landing/news.locals.compiled");

  module.exports = function(app) {
    return app.get('/haberler', function(req, res, next) {
      locals(app);
      return res.status(200).render('landing/bulletins');
    });
  };

}).call(this);
