(function() {
  var locals;

  locals = require("../../locals/landing/activities.locals.compiled");

  module.exports = function(app) {
    return app.get('/aktiviteler', function(req, res, next) {
      locals(app);
      return res.status(200).render('landing/bulletins');
    });
  };

}).call(this);
