(function() {
  var locals;

  locals = require("../../locals/landing/announcements.locals.compiled");

  module.exports = function(app) {
    return app.get('/duyurular', function(req, res, next) {
      locals(app);
      return res.status(200).render('landing/bulletins');
    });
  };

}).call(this);
