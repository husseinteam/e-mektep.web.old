(function() {
  var locals;

  locals = require("../../locals/landing/academic-calendar.locals.compiled");

  module.exports = function(app) {
    return app.get('/akademik-takvim', function(req, res, next) {
      locals(app);
      return res.status(200).render('landing/academic-calendar');
    });
  };

}).call(this);
