(function() {
  var app, bodyParser, configurator, express, hamlc, multer, partials, path, port, referbase, upload;

  express = require("express");

  app = express();

  path = require('path');

  bodyParser = require('body-parser');

  multer = require('multer');

  hamlc = require('haml-coffee').__express;

  partials = require('express-partials');

  referbase = function(dir) {
    return path.join(__dirname, "..", "..", "app", dir);
  };

  app.use(express["static"](referbase("public")));

  app.use(bodyParser.json());

  app.use(bodyParser.urlencoded({
    extended: true
  }));

  upload = multer({
    dest: referbase('uploads')
  });

  partials.register('.hamlc', function(src, opts) {
    return hamlc(opts.filename, opts, function(err, result) {
      if (err) {
        console.log(err);
      }
      return result;
    });
  });

  app.use(partials());

  app.engine('hamlc', hamlc);

  app.set('view engine', 'hamlc');

  app.set('layout', 'layout');

  app.set("views", referbase("views"));

  app.enable("view cache");

  app.locals.uglify = true;

  configurator = require(path.join(__dirname, "route/routing.compiled"));

  configurator(app);

  port = parseInt(process.env.PORT || 3000, 10);

  app.listen(port, function() {
    return console.log("E-Mektep " + port + " Port'undan Dinlemede!");
  });

}).call(this);
