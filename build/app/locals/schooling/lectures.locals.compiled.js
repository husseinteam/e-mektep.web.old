(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.color = "blue";
    app.locals.route = "tedrisat/dersler";
    app.locals.Header = "Verilen Dersler";
    return layoutvm(app);
  };

}).call(this);
