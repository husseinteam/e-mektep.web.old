(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.TableHeader = "Öğrencinin Dersleri";
    app.locals.route = "/tedrisat/ogrenciler/:id";
    return layoutvm(app);
  };

}).call(this);
