(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.color = "green";
    app.locals.route = "haberler";
    app.locals.Header = "Haberler";
    app.locals.ReadMore = "Devamını Okuyun";
    return layoutvm(app);
  };

}).call(this);
