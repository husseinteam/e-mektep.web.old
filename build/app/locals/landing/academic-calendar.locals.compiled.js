(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.color = "teal";
    app.locals.route = "akademik-takvim";
    app.locals.Header = "Akademik Takvim";
    return layoutvm(app);
  };

}).call(this);
