(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.color = "yellow";
    app.locals.route = "aktiviteler";
    app.locals.Header = "Aktiviteler";
    app.locals.ReadMore = "Devamını Okuyun";
    return layoutvm(app);
  };

}).call(this);
