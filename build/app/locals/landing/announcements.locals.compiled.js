(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.color = "red";
    app.locals.route = "duyurular";
    app.locals.Header = "Duyurular";
    app.locals.ReadMore = "Devamını Okuyun";
    return layoutvm(app);
  };

}).call(this);
