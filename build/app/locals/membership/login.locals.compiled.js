(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.Header = "Oturum Açın";
    app.locals.SubmitTitle = "Giriş";
    app.locals.LoginPlaceholder = "TCKN No";
    app.locals.PasswordPlaceholder = "Şifre";
    app.locals.route = "/uyelik/oturumac";
    return layoutvm(app);
  };

}).call(this);
