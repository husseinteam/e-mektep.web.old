(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.Header = "Şifremi Unuttum";
    app.locals.ContactPlaceholder = "İletişim E-Postası";
    app.locals.PasswordPlaceholder = "Şifre";
    app.locals.RememberedTitle = "Tıklayın";
    app.locals.RememberedSubtitle = "Şifrenizi Hatırladınız mı?";
    app.locals.route = "/uyelik/sifremiunuttum";
    return layoutvm(app);
  };

}).call(this);
