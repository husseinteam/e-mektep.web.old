(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.Header = "Aktivasyonunuzu Tamamlayın";
    app.locals.FirstNamePlaceholder = "Ad";
    app.locals.LastNamePlaceholder = "Soyad";
    app.locals.TCKNPlaceholder = "TCKN";
    app.locals.PasswordPlaceholder = "Şifre";
    app.locals.ActivationTokenPlaceholder = "Aktivasyon Anahtarı";
    app.locals.SubmitTitle = "Gönder";
    app.locals.ActivatedTitle = "Şöyle Buyrun";
    app.locals.ActivatedSubtitle = "Kaydınız Tamamlandı mı?";
    app.locals.route = "/uyelik/aktivasyon";
    return layoutvm(app);
  };

}).call(this);
