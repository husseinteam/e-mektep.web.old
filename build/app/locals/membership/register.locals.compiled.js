(function() {
  var layoutvm;

  layoutvm = require("../layout.locals.compiled");

  module.exports = function(app) {
    app.locals.Header = "Kayıt Olun";
    app.locals.SubmitTitle = "Kaydolun";
    app.locals.ContactPlaceholder = "İletişim E-Postası";
    app.locals.PasswordPlaceholder = "Şifre";
    app.locals.route = "/uyelik/yenikayit";
    return layoutvm(app);
  };

}).call(this);
