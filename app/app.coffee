express			= require "express"
app					= express()
path				= require 'path'
bodyParser	= require 'body-parser'
multer			= require 'multer'
hamlc = require('haml-coffee').__express
partials = require('express-partials')

referbase = (dir) -> 
	path.join __dirname, "..", "..", "app", dir

app.use (express.static (referbase "public"))
app.use bodyParser.json() 
app.use bodyParser.urlencoded extended: true
upload = multer { dest: referbase 'uploads' } 

partials.register '.hamlc', (src, opts)->
  hamlc opts.filename, opts, (err, result)->
    console.log err if err
    return result
app.use partials()

app.engine 'hamlc', hamlc
app.set 'view engine', 'hamlc'
app.set 'layout', 'layout'
app.set "views", (referbase "views")
app.enable "view cache"
app.locals.uglify = true

configurator = require path.join __dirname, "route/routing.compiled"
configurator app

port = parseInt process.env.PORT or 3000, 10
app.listen port, ->
	console.log "E-Mektep #{port} Port'undan Dinlemede!"
