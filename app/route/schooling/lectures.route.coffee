locals = require "../../locals/schooling/lectures.locals.compiled"

module.exports = (app) ->
	app.get '/tedrisat/dersler', (req, res, next) ->
		locals app
		res.status(200).render 'schooling/lectures'
	