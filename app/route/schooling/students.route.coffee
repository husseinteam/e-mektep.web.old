locals = require "../../locals/schooling/students.locals.compiled"

module.exports = (app) ->
	app.get '/tedrisat/ogrenciler/:id', (req, res, next) ->
		locals app
		res.status(200).render 'schooling/students'
	