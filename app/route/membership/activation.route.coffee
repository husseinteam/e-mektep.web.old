locals = require "../../locals/membership/activation.locals.compiled"

module.exports = (app) ->

	app.get '/uyelik/aktivasyon/:token?', (req, res, next) ->
		locals app
		res.status(200).render 'membership/activation'
