locals = require "../../locals/membership/register.locals.compiled"

module.exports = (app) ->
	
	app.get '/uyelik/yenikayit/:token?', (req, res, next) ->
		locals app
		res.status(200).render 'membership/register'
	