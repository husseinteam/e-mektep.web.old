locals = require "../../locals/membership/repass.locals.compiled"

module.exports = (app) ->
	
	app.get '/uyelik/sifremiunuttum/:token?', (req, res, next) ->
		locals app
		res.status(200).render 'membership/repass'
	