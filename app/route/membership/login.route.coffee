loginapi = require "../../../api/membership/login.api.compiled"
locals = require "../../locals/membership/login.locals.compiled"

module.exports = (app) ->

	app.get '/uyelik/oturumac', (req, res, next) ->
		locals app
		res.status(200).render 'membership/login'

	app.post '/uyelik/oturumac/post', (req, res) ->
		cb = 
			onConnection: (conn) ->
				conn.sendUTF JSON.stringify
					logon: req.param('logon', "25871077074");
					password: req.param('password', "25871077074")
			onMessage: (message) ->
				{done, token, uid} = message
				res.status(200).render 'membership/login', message
			failback: (error) ->
				console.log "error is #{error}"
		loginapi cb