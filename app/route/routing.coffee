glob = require "glob" 
options = 
	nodir: true
	cwd : "#{__dirname}"  
module.exports = (app) ->
	files = glob.sync "**/*.route.compiled.js", options
	routes = (require "#{__dirname}/#{file}" for file in files)
	for route in routes
		do (route) ->
			route app

