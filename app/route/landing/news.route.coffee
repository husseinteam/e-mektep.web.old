locals = require "../../locals/landing/news.locals.compiled"

module.exports = (app) ->
	app.get '/haberler', (req, res, next) ->
		locals app
		res.status(200).render 'landing/bulletins'
	