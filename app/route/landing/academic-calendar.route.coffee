locals = require "../../locals/landing/academic-calendar.locals.compiled"

module.exports = (app) ->
	app.get '/akademik-takvim', (req, res, next) ->
		locals app
		res.status(200).render 'landing/academic-calendar'
	