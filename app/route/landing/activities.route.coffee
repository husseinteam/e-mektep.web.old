locals = require "../../locals/landing/activities.locals.compiled"

module.exports = (app) ->
	app.get '/aktiviteler', (req, res, next) ->
		locals app
		res.status(200).render 'landing/bulletins'
	