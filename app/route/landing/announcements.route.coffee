locals = require "../../locals/landing/announcements.locals.compiled"

module.exports = (app) ->
	app.get '/duyurular', (req, res, next) ->
		locals app
		res.status(200).render 'landing/bulletins'
	