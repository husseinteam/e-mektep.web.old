locals = require "../locals/layout.locals.compiled"

module.exports = (app) ->
	app.get '/', (req, res) ->
		locals app
		res.render 'index'
	