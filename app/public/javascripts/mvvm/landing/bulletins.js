require(['knockout', 'common/extensions'],
  function (ko, $) {

  	var bulletins = ko.observableArray();

  	var bulletinsvm = {
  		activate: activate,
  		bindingComplete: bindingComplete,
  		bulletins: bulletins
  	};

  	function transformBulletin(b) {
  		var dizi = b.Etiketler.split(",");
  		dizi = dizi.filter(function (e) { return e.length > 0; });
  		Object.defineProperty(b, "EtiketlerDizi", { value: dizi.length > 0 ? dizi : [] });
  		b.YayimTarihi = b.YayimTarihi.format();
  		return b;
  	}

  	function activate() {
  		if (bulletins().length == 0) {
  			$('#bulletins-host').sockify('karsilama/' + route,
				{ token: extract('token') },
        function (message) {
        	if (!message.done) {
        		$("#generic-modal").speak(false, message.messages[0], function () {
        			document.location = "/uyelik/oturumac";
        		});
        	} else {
        		bulletinsvm.bulletins.push(transformBulletin(message.data));
        	}
        },
        function (dw) {
        	//lastback
        	$('.bulletin-tag').popup();
        },
				function (apiresp) {
					if (!apiresp.done) {
						$("#generic-modal").speak(false, apiresp.messages[0], function () {
							document.location = "/uyelik/oturumac";
						});
					} else {
						bulletinsvm.bulletins(apiresp.data.map(function (b) { return transformBulletin(b); }));
					}
				}
      );
  		}
  	}
  	function bindingComplete() {
  		setTimeout(function () { $('#bulletins-host').transition('pulse'); }, 10);
  	}
  	activate();
  	ko.applyBindings(bulletinsvm);
  	bindingComplete();

  });