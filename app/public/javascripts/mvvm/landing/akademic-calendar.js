require(['knockout', 'common/extensions', 'common/gridify'],
  function (ko, $, Gridify) {

  	function transformDonem(donem) {
  		switch (donem) {
  			case 1: return "yariyil1";
  			case 2: return "yariyil2";
  			case 3: return "yazogretimi";
  			case 4: return "Donem1";
  			case 5: return "Donem2";
  			case 6: return "Donem3";
  			case 7: return "Donem4";
  			case 8: return "Donem5";
  			case 9: return "Donem6";
  			default: return "Tanımsız Dönem";
  		}
  	}
  	var items = ko.observableArray();
  	var gridViewModel = new Gridify({
  		selector: '#calendar-host',
  		data: items,
  		exportTitle: 'akademik-takvim',
  		sortFunction: function () {
  			items.sort(function (a, b) {
  				return (a.Baslangic == b.Baslangic) ? 0 : ((a.Baslangic > b.Baslangic) ? 1 : -1);
  			});
  		},
  		columns: [
            { headerText: "Başlangıç", rowText: "Baslangic", transformer: function (item) { return item && item.Baslangic.format(); } },
            { headerText: "Bitiş", rowText: "Bitis", transformer: function (item) { return item && item.Bitis.format(); } },
            { headerText: "Etkinlik", rowText: "Etkinlik" },
            { headerText: "Fakülte", rowText: "FakulteAdi" },
            { headerText: "Üniversite", rowText: "UniversiteAdi" },
            { headerText: "Akademik Dönem", rowText: 'AkademikDonem', transformer: function (item) { return item && transformDonem(item.AkademikDonem); } }
  					//{ headerText: "Price", rowText: function (item) { return "$" + item.price.toFixed(2); } }
        ],
  		pageSize: 5
  	});
  	var vm = {
  		activate: activate,
  		bindingComplete: bindingComplete,
  		items: items,
  		addItem: function () {
  			//items.push({ name: "New item", sales: 0, price: 100 });
  		},
  		gridViewModel: gridViewModel
  	};

  	function activate() {
			if (items().length == 0) {
  			$('#calendar-host').sockify('karsilama/akademik-takvim',
				{ token: extract('token') },
        function (message) {
        	if (!message.done) {
        		$("#generic-modal").speak(false, message.messages[0], function () {
        			document.location = "/uyelik/oturumac";
        		});
        	} else {
        		vm.items.push(message.data);
        	}
        },
        function (dw) {
        	gridViewModel.setColumnFilters();
        },
				function (apiresp) {
					if (!apiresp.done) {
						$("#generic-modal").speak(false, apiresp.messages[0], function () {
							document.location = "/uyelik/oturumac";
						});
					} else {
						vm.items(apiresp.data);
					}
				}
      );
  		}
  	}
  	function bindingComplete() {
  		setTimeout(function () { $('#calendar-host').transition('pulse'); }, 10);
  	}
  	activate();
  	ko.applyBindings(vm);
  	bindingComplete();

  });