require(['knockout', 'common/extensions'],
  function (ko, $) {

  	var vm = {
  		header: ko.observable("Kaydolun"),
  		contact: ko.observable("lampiclobe@outlook.com"),
  		password: ko.observable("1q2w3e4r5t"),
  		messages: {
  			RegisterDone: ko.observable("İşlem Başarılı. Kaydınızı Tamamlamak İçin E-Posta'nızı Kontrol Ediniz"),
  			ContactValidationFailed: ko.observable("Lütfen Geçerli Bir E-Posta Giriniz"),
  			PasswordValidationFailed: ko.observable("Lütfen Geçerli Bir Şifre Giriniz")
  		},
  		loginLinkClicked: function () {
  			document.location = "/uyelik/oturumac";
  		},
  		contactValidator: function () {
  			var self = vm;
  			var re = /^[^\d][a-zA-Z0-9_\.\-]*@[a-zA-Z0-9_\.\-]+\.\w{2,4}$/;
  			return re.test(self.contact());
  		},
  		passwordValidator: function () {
  			var self = vm;
  			return self.password().length > 3;
  		},
  		submitForm: function () {
  			var self = vm;
  			$('#register-submit').sockify('ms/register',
          vm.unveil(ko, function (rapp) {
          	return {
          		Contact: rapp.contact,
          		Password: rapp.password
          	};
          }),
          function (lcred) {
          	$("#generic-modal").speak(lcred.done, lcred.done ? self.messages.RegisterDone() : lcred.messages[0]);
          });
  		}
  	};
  	function bindingComplete() {
  		setTimeout(function () { $($('form').parents('div')[0]).transition('pulse'); }, 10);
  	}
  	ko.applyBindings(vm);
  	bindingComplete();
  });