require(['knockout', 'common/extensions'],
  function (ko, $) {

  	var vm = {
  		activate: activate,
  		bindingComplete: bindingComplete,
  		header: ko.observable("Aktivasyonunuzu Tamamlayın"),
  		ad: ko.observable("Özgür"),
  		soyad: ko.observable("Sönmez"),
  		tckn: ko.observable("25871077074"),
  		password: ko.observable("1q2w3e4r5t"),
  		activationToken: ko.observable(),
  		messages: {
  			AdValidationFailed: ko.observable("Lütfen Geçerli Bir Ad Giriniz"),
  			SoyadValidationFailed: ko.observable("Lütfen Geçerli Bir Soyad Giriniz"),
  			TCKNValidationFailed: ko.observable("Lütfen 11 Haneli TC No Giriniz"),
  			PasswordValidationFailed: ko.observable("Lütfen Geçerli Bir Şifre Giriniz"),
  			TokenValidationFailed: ko.observable("Lütfen Geçerli Bir Aktivasyon Anahtarı Giriniz")
  		},
  		loginLinkClicked: function () {
  			document.location = "/uyelik/oturumac";
  		},
  		adValidator: function () {
  			var self = vm;
  			return self.ad().match(/[\d+]/g) == null;
  		},
  		soyadValidator: function () {
  			var self = vm;
  			return self.soyad().match(/[\d+]/g) == null;
  		},
  		tcknValidator: function () {
  			var self = vm;
  			return self.tckn().length == 11;
  		},
  		passwordValidator: function () {
  			var self = vm;
  			return self.password().length > 3;
  		},
  		tokenValidator: function () {
  			var self = vm;
  			return self.activationToken().length > 0;
  		},
  		submitForm: function () {
  			var self = vm;
  			$('#activate-submit').sockify('ms/activate',
          vm.unveil(ko, function (rapp) {
          	return {
          		TCKN: rapp.tckn,
          		Password: rapp.password,
          		FirstName: rapp.ad,
          		LastName: rapp.soyad,
          		ActivationToken: rapp.activationToken
          	};
          }),
          function (lcred) {
          	$("#generic-modal").speak(lcred.done, lcred.messages[0], function() {
          		if (lcred.done) {
          			document.location = "/uyelik/oturumac";
          		}
          	});
          });
  		}
  	};

  	function activate(token) {
  		if (token.replace("/", "")) {
  			vm.activationToken(token.replace("/", ""));
  		}
  	}
  	function bindingComplete() {
  		setTimeout(function () { $($('form').parents('div')[0]).transition('pulse'); }, 10);
  	}
  	activate(window.location.pathname.replace("/uyelik/aktivasyon", ""));
  	ko.applyBindings(vm);
  	bindingComplete();

  });