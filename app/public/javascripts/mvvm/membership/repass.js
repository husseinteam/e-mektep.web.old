require(['knockout', 'common/extensions'],
  function (ko, $) {

  	var vm = {
  		header: ko.observable("Şifremi Unuttum"),
  		contact: ko.observable("lampiclobe@outlook.com"),
  		password: ko.observable(""),
  		mailLinkClicked: ko.observable(false),
  		token: ko.observable(),
  		submitTitle: ko.pureComputed(function () {
  			return vm.token() ? 'Şifrenizi Güncelleyin' : "Size E-Posta Gönderelim";
  		}, vm),
  		rememberedSubtitle: ko.pureComputed(function () {
  			return vm.token() ? 'Güncelleme Tamamlandı mı?' : "Şifrenizi Hatırladınız mı?";
  		}, vm),
  		messages: {
  			ContactValidationFailed: ko.observable("Lütfen Geçerli Bir E-Posta Giriniz"),
  			PasswordValidationFailed: ko.observable("Lütfen Geçerli Bir Şifre Giriniz")
  		},
  		loginLinkClicked: function () {
  			document.location = "/uyelik/oturumac";
  		},
  		contactValidator: function () {
  			var self = vm;
  			var re = /^[^\d][a-zA-Z0-9_\.\-]*@[a-zA-Z0-9_\.\-]+\.\w{2,4}$/;
  			return re.test(self.contact());
  		},
  		passwordValidator: function () {
  			var self = vm;
  			return self.password().length > 3;
  		},
  		submitForm: function () {
  			var self = vm;
  			$('#repass-submit').sockify(self.mailLinkClicked() ? 'ms/repass' : 'ms/recover',
          vm.unveil(ko, function (rapp) {
          	return {
          		Contact: rapp.contact,
          		Password: rapp.password,
          		RecoveryToken: self.token()
          	};
          }),
          function (lcred) {
          	$("#generic-modal").speak(lcred.done, lcred.messages[0]);
          });
  		}
  	};

  	function activate(token) {
  		var self = vm;
  		self.token(token.replace("/", ""));
  		self.mailLinkClicked(token.replace("/", "") != "");
  	}
  	function bindingComplete() {
  		setTimeout(function () { $($('form').parents('div')[0]).transition('pulse'); }, 10);
  	}

  	activate(window.location.pathname.replace("/uyelik/sifremiunuttum", ""));
  	ko.applyBindings(vm);
  	bindingComplete();

  });