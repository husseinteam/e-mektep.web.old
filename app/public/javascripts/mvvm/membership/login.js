require(['knockout', 'common/extensions'],
  function (ko, $) {

  	var vm = {
  		header: ko.observable("Oturum Açın"),
  		logon: ko.observable("25871077074"),
  		password: ko.observable("1q2w3e4r5t"),
  		registerLinkClicked: function () {
  			document.location = "/uyelik/yenikayit";
  		},
  		messages: {
  			LogonValidationFailed: ko.observable("Lütfen 11 Haneli TC No Giriniz"),
  			PasswordValidationFailed: ko.observable("Lütfen Geçerli Bir Şifre Giriniz")
  		},
  		logonValidator: function () {
  			var self = vm;
  			return self.logon().length == 11;
  		},
  		passwordValidator: function () {
  			var self = vm;
  			return self.password().length > 3;
  		},
  		submitForm: function () {
  			var self = vm;
  			$('#login-submit').sockify('ms/login',
          { logon: self.logon(), password: self.password() },
          function (lcred) {
          	if (lcred.done) {
          		persist('token', lcred.token, 1);
          		persist('uid', parseInt(lcred.data), 1);
          	}
          	$("#generic-modal").speak(lcred.done, lcred.messages[0], function (el) {
          		if (lcred.done) {
          			document.location = '/';
          		}
          	});
          });
  		}
  	};
  	function bindingComplete() {
  		setTimeout(function () { $($('form').parents('div')[0]).transition('pulse'); }, 10);
  	}

  	ko.applyBindings(vm);
  	bindingComplete();

  });