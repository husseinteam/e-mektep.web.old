require(['knockout', 'common/extensions'],
  function (ko, $) {


  	var vm = {
  		activate: activate,
  		bindingComplete: bindingComplete
  	};

  	function activate() {

  	}
  	function bindingComplete() {
  		$('img.ui.image.small').mouseover(function () {
  			$(this).transition('horizontal flip').transition('horizontal flip');
  		});
  		$('a.ui.large.image > img').popup();
  	}
  	activate();
  	ko.applyBindings(vm);
  	bindingComplete();

  });