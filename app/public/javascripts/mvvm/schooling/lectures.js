require(['knockout', 'common/extensions', 'common/gridify'],
  function (ko, $, Gridify) {

  	var items = ko.observableArray();
  	var gridViewModel = new Gridify({
  		selector: '#dersler-well',
  		data: items,
  		exportTitle: 'verilen-dersler',
  		sortFunction: function () {
  			items.sort(function (a, b) {
  				return (a.DersAdi == b.DersAdi) ? 0 : ((a.DersAdi > b.DersAdi) ? 1 : -1);
  			});
  		},
  		columns: [
            { headerText: "Ders Kodu", rowText: "DersKodu" },
            { headerText: "Ders Adı", rowText: "DersAdi" },
            { headerText: "Teorik Saat", rowText: "TeorikSaat" },
            { headerText: "Uygulama Saat", rowText: "UygulamaSaat" },
            { headerText: "Ders Kredisi", rowText: "Kredi" },
            { headerText: "Öğretim Üyesi", rowText: 'OgretimUyesi' },
            { headerText: "Dersi Alan Öğrenciler", rowText: 'OgrencileriCommand', command: function (item) {
            	document.location = "/tedrisat/ogrenciler/" + item.DersID;
            } 
            }
  		//{ headerText: "Price", rowText: function (item) { return "$" + item.price.toFixed(2); } }
        ],
  		pageSize: 5
  	});

  	var vm = {
  		activate: activate,
  		bindingComplete: bindingComplete,
  		items: items,
  		addItem: function () {
  			//items.push({ name: "New item", sales: 0, price: 100 });
  		},
  		gridViewModel: gridViewModel
  	};

  	function activate() {
  		if (items().length == 0) {
  			$('#dersler-well').sockify('tedrisat/dersler',
				{ token: extract('token') },
        function (message) {
        	if (!message.done) {
        		$("#generic-modal").speak(false, message.messages[0], function () {
        			document.location = "/uyelik/oturumac";
        		});
        	} else {
        		vm.items.push(message.data);
        	}
        },
        function (dw) {
        	gridViewModel.setColumnFilters();
        },
				function (apiresp) {
					if (!apiresp.done) {
						$("#generic-modal").speak(false, apiresp.messages[0], function () {
							document.location = "/uyelik/oturumac";
						});
					} else {
						vm.items(apiresp.data);
					}
				}
      );
  		}
  	}
  	function bindingComplete() {
  		setTimeout(function () { $('#dersler-well').transition('pulse'); }, 10);
  	}
  	activate();
  	ko.applyBindings(vm);
  	bindingComplete();

  });