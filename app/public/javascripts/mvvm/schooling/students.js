require(['knockout', 'common/extensions', 'common/gridify'],
  function (ko, $, Gridify) {

		function transformKayitTipi(kayitTipi) {
  		switch (kayitTipi) {
  			case 1: return "ÖSS İle Gelen";
  			case 2: return "Yatay Geçiş İle Gelen";
  			case 3: return "Ek Kontenjan İle Gelen";
  			default: return "Tanımsız Kayıt Tipi";
  		}
  	}

  	var items = ko.observableArray();
  	function getDersId() {
  		return document.location.pathname.substr(document.location.pathname.lastIndexOf('/') + 1);
  	}
  	var gridViewModel = new Gridify({
  		selector: '#dersler-well',
  		exportTitle: 'ogrenciler',
  		data: items,
  		sortFunction: function () {
  			items.sort(function (a, b) {
  				return (a.OgrenciAdi == b.OgrenciAdi) ? 0 : ((a.OgrenciAdi > b.OgrenciAdi) ? 1 : -1);
  			});
  		},
  		columns: [
            { headerText: "Öğrenci No", rowText: "OgrenciNo" },
            { headerText: "Adı", rowText: "OgrenciAdi" },
            { headerText: "Soyadı", rowText: "OgrenciSoyadi" },
            { headerText: "Kayıt Tipi", rowText: "OgrenciKayitTipi", transformer: function (item) { return item && transformKayitTipi(item.OgrenciKayitTipi); } },
            { headerText: "Kayıt Yılı", rowText: "OgrenciKayitYili" }
        ],
  		pageSize: 5
  	});

  	var vm = {
  		activate: activate,
  		bindingComplete: bindingComplete,
  		items: items,
  		addItem: function () {
  			//items.push({ name: "New item", sales: 0, price: 100 });
  		},
  		gridViewModel: gridViewModel
  	};

  	function activate() {
  		if (items().length == 0) {
  			$('#dersler-well').sockify('tedrisat/ogrenciler/' + getDersId(),
        { token: extract('token') },
        function (message) {
        	if (!message.done) {
        		$("#generic-modal").speak(false, message.messages[0], function () {
        			document.location = "/uyelik/oturumac";
        		});
        	} else {
        		vm.items.push(message.data);
        	}
        },
        function (dw) {
        	gridViewModel.setColumnFilters();
        },
				function (apiresp) {
					if (!apiresp.done) {
						$("#generic-modal").speak(false, apiresp.messages[0], function () {
							document.location = "/uyelik/oturumac";
						});
					} else {
						vm.items(apiresp.data);
					}
				}
      );
  		}
  	}
  	function bindingComplete() {
  		setTimeout(function () { $('#dersler-well').transition('pulse'); }, 10);
  	}
  	activate();
  	ko.applyBindings(vm);
  	bindingComplete();

  });