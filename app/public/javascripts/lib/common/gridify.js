﻿define(['knockout', 'common/extensions'],
  function (ko, $) {
  	var Gridify = function (configuration) {
  		var self = this;
  		self.selector = configuration.selector;
  		self.exportTitle = configuration.exportTitle;
  		self.rawData = configuration.data;
  		self.filteredData = ko.observableArray();
  		self.columnFilters = ko.observableArray();
  		self.pageSize = configuration.pageSize || 5;
  		self.currentPageIndex = ko.observable(0);
  		self.currentPageIndex.subscribe(function (newValue) {
  			$(self.selector).transition('fade up').transition('fade up', function () { $('.popped').popup(); });
  		});
  		self.columns = configuration.columns;
  		self.pureColumns = self.columns.filter(function (c) { return c.command == undefined });
  		self.rawData.subscribe(function (newData) {
  			self.filteredData(newData);
  		});

  		self.sortRows = configuration.sortFunction;

  		self.jumpToFirstPage = function () {
  			self.currentPageIndex(0);
  		};

  		self.transform = function (text, element) {
  			var transformed = text
  			if (transformed) {
  				if (text.toString().slice(-3) == "...") {
  				} else if (text.measureRect().width > $(element).width()) {
  					var transformed = text.ellipsis($(element).width());
  					$(element).tips(text);
  				}
  			}
  			return transformed || 'Veri Yok';
  		}

  		self.exportToExcel = function () {
  			$(self.selector).exportify('export/toexcel', {
  				WorksheetName: self.exportTitle,
  				TableColumns: self.pureColumns,
  				TableRows: self.rawData().map(function (d) {
  					var cli = self.pureColumns.filter(function (c) { return c.transformer });
  					cli.forEach(function (ct) { d[ct.rowText] = ct.transformer(d); });
  					return d;
  				}),
  				ExportTitle: self.exportTitle
  			});
  		}
  		self.exportToPdf = function () {
  			$(self.selector).exportify('export/topdf', {
  				WorksheetName: self.exportTitle,
  				TableColumns: self.pureColumns,
  				TableRows: self.rawData().map(function (d) {
  					var cli = self.pureColumns.filter(function (c) { return c.transformer });
  					cli.forEach(function (ct) { d[ct.rowText] = ct.transformer(d); });
  					return d;
  				}),
  				ExportTitle: self.exportTitle
  			});
  		}
  		self.exportToWord = function () {
  			$(self.selector).exportify('export/toword', {
  				WorksheetName: self.exportTitle,
  				TableColumns: self.pureColumns,
  				TableRows: self.rawData().map(function (d) {
  					var cli = self.pureColumns.filter(function (c) { return c.transformer });
  					cli.forEach(function (ct) { d[ct.rowText] = ct.transformer(d); });
  					return d;
  				}),
  				ExportTitle: self.exportTitle
  			});
  		}

  		self.setColumnFilters = function () {
  			var gridHorizontal = [];
  			var cols = self.columns;
  			var data = self.rawData();
  			for (var i = 0; i < data.length; i++) {
  				var d = data[i];
  				var cells = cols.map(function (col, j) { return col.transformer ? col.transformer(d) : d[col.rowText]; });
  				gridHorizontal.push(cells);
  			}
  			var gridVertical = [];
  			for (var i = 0; i < cols.length; i++) {
  				var column = ['Hepsi..'];
  				for (var j = 0; j < gridHorizontal.length; j++) {
  					var cells = gridHorizontal[j];
  					column.push(cells[i]);
  				}
  				gridVertical.push({ colName: cols[i].rowText, colTran: cols[i].transformer, uniqueCells: column.getUnique(), selectedFilter: ko.observable(), isCommand: cols[i].command != undefined });
  			}
  			self.columnFilters(gridVertical);
  			self.columnFilters().forEach(function (cf) {
  				cf.selectedFilter.subscribe(function (newValue) {
  					var rowText = cf.colName;
  					var transformer = cf.colTran;
  					self.filteredData(newValue == 'Hepsi..' ? self.rawData() :
							self.rawData().filter(function (d) { return transformer ? (transformer(d) == newValue) : (d[rowText] == newValue); }));
  				});
  			});
  			$('.ui.dropdown').dropdown();
  			$('.popped').popup();
  		};

  		self.itemsOnCurrentPage = ko.computed(function () {
  			var startIndex = self.pageSize * self.currentPageIndex();
  			return self.filteredData.slice(startIndex, startIndex + self.pageSize);
  		});

  		self.maxPageIndex = ko.computed(function () {
  			return Math.ceil(ko.utils.unwrapObservable(self.filteredData).length / self.pageSize) - 1;
  		});
  	};

  	return Gridify;

  });