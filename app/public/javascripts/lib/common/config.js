﻿define(function () {

	var Config = function () {
		var self = this;
		this.debug = false;
		this.toWS = function (route) {
			return (this.debug ? 'ws://192.168.1.2:114/ws/' : 'ws://app.lampiclobe.com/ws/') + route;
		};
		this.toApi = function (route) {
			return (this.debug ? 'http://192.168.1.2:114/api/' : 'http://app.lampiclobe.com/api/') + route;
		};
	};
	return new Config();

});