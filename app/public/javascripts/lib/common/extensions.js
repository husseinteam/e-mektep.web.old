﻿define(['knockout', 'jquery', 'common/config', 'semantic'],
  function (ko, $, cfg, sui) {
  	$.fn.extend({
  		loading: function (on) {
  			if (on) {
  				$(this).addClass('ui').addClass('segment');
  				$('<div id="__loader" class="ui active inverted dimmer"><div class="ui text loader">Yükleniyor..</div></div>')
          .width($(this).width())
            .appendTo($(this));
  			} else {
  				$('#__loader').remove();
  				$(this).removeClass('ui').removeClass('segment');
  			}
  		},
  		spinload: function (spin) {
  			if ($(this).is("input") || $(this).is("button") || $(this).hasClass('button')) {
  				if (spin) {
  					$(this).attr('disabled', 'disabled').addClass('loading')
  				} else {
  					$(this).removeAttr('disabled').removeClass('loading');
  				}
  			} else {
  				$(this).loading(spin);
  			}
  		},
  		invalidates: function (valid, title) {
  			var field = $($(this).closest('.field')[0]);
  			if (valid) {
  				field.removeClass('error');
  				$(this).tips();
  			} else {
  				field.addClass('error');
  				$(this).tips(title);
  			}
  		},
  		tips: function (content) {
  			var self = $(this);
  			if (content) {
  				$(self).attr({ 'data-content': content, 'data-variation': 'tiny' }).popup({ on: 'hover' });
  			} else {
  				$(self).removeAttr('data-content').removeAttr('data-variation');
  			}
  		},
  		speak: function (done, content, hiddenCallback) {
  			$(this).find('.header:first').html(done ? 'Başarılı İşlem' : 'Başarısız İşlem');
  			$(this).find('.content:first').html(content + (done ? '' : ' Lütfen Yeniden Deneyiniz'));
  			if (!done) {
  				$(this).find('.ui.primary.approve.button').removeClass('primary').addClass('negative');
  				$(this).find('.header:first').css({ color: 'red' });
  				$(this).find('.content:first').removeClass("ui success message").addClass('ui error message');
  			} else {
  				$(this).find('.header:first').css({ color: 'green' });
  				$(this).find('.content:first').removeClass("ui error message").addClass('ui success message');
  				$(this).find('.ui.primary.approve.button').removeClass('negative').addClass('primary');
  			}
  			$(this).modal({
  				closable: true,
  				onHidden: function () {
  					hiddenCallback && hiddenCallback();
  				}
  			}).modal('show');
  		}
  	});
  	$.fn.extend({
  		sockify: function (route, data, onMessage, onClose, onNoWebSocket) {
  			var self = $(this);
  			$(self).spinload(true);
  			var websocket = null;
  			var timeoutExpired = false;
  			try {
  				websocket = new WebSocket(cfg.toWS(route));
  			} catch (ex) {
  				$(self).spinload(false);
  				$("#generic-modal").speak(false, 'Servise Ulaşılamıyor');
  				return;
  			}
  			setTimeout(function () {
  				timeoutExpired = true;
  			}, 7000);
  			websocket.onmessage = function (evt) {
  				var message = JSON.parse(evt.data);
  				onMessage && onMessage(message);
  				$(self).spinload(false);
  			}
  			websocket.onerror = function (evt) {
  				$(self).postify(route, data, function (response) {
  					(onNoWebSocket || onMessage)(response);
  					onClose && onClose($(self));
  				}, function (err) {
  					console.log('error', err);
  					$("#generic-modal").speak(false, 'Servis Hatası Oluştu!');
  				});

  			};
  			websocket.onclose = function (evt) { onClose && onClose($(self)); $(self).spinload(false); };
  			websocket.onopen = function (evt) {
  				while (websocket.readyState !== 1) { }
  				if (timeoutExpired) {
  					$("#generic-modal").speak(false, 'Zaman Aşımı.', function () {
  						document.location = document.location;
  						$(self).spinload(false);
  					});
  				} else {
  					websocket.send(JSON.stringify(data));
  				}
  			};

  		},
  		postify: function (route, data, doneback, failback) {
  			var self = $(this);
  			$(self).spinload(true);
  			$.post(cfg.toApi(route), data)
					.done(function (resp) {
						doneback(resp);
					})
					.fail(function (err) {
						failback && failback(err);
					})
					.always(function () {
						console.log("finished");
						$(self).spinload(false);
					});
  		},
  		getify: function (route, doneback, failback) {
  			$.get(cfg.toApi(route))
					.done(function (resp) {
						doneback(resp);
					})
					.fail(function (err) {
						failback && failback(err);
					})
					.always(function () {
						console.log("finished");
					});
  		},
  		exportify: function (route, data) {
  			$(this).postify(route, data,
				function (resp) {
					saveFile(resp.url, resp.name);
					console.log('success: ', resp.url);
				}, function (err) {
					console.log('error: ', err);
				});
  		}
  	});
  	ko.bindingHandlers.validates = {
  		init: function (element, valueAccessor) {
  			var options = valueAccessor() || {};
  			var submitter = $('#' + options.submitter);
  			submitter.click(function (e) {
  				if ($(element).is(':visible')) {
  					if ($(element).val()) {
  						if (options.validator && options.validator()) {
  							$(element).invalidates(true, options.validationTitle());
  							return true;
  						} else {
  							$(element).invalidates(false, options.validationTitle());
  							e.stopImmediatePropagation();
  							return false;
  						}
  					} else {
  						$(element).invalidates(false, options.validationTitle());
  						e.stopImmediatePropagation();
  						return false;
  					}
  				}
  			});
  		},
  		update: function (element, valueAccessor) {
  			//ko.utils.unwrapObservable(valueAccessor());
  		}
  	};
  	$(document).ready(function () {
  		if (extract('token') && extract('token').length > 0) {
  			$('#signout').show();
  		} else {
  			$('#signout').hide();
  		}
  		$('.popmeup').popup();
  	});
  	return $;
  });