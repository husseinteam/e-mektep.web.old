﻿
requirejs.config({
	baseUrl : "/javascripts/lib/",
	paths: {
		'knockout': 'knockout/knockout-3.4.0',        
		'semantic': 'semantic/semantic.min', 
		'common': 'common',
		'jquery': 'jquery',
		'mvvm': 'mvvm'
	},
	shim: {
		'semantic': {
			deps: ['jquery'],
			exports: 'semantic'
		},
		'common/downloader': {
			deps: ['jquery']
		}
	}
});
