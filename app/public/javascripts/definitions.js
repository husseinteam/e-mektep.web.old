﻿Object.defineProperty(Object.prototype, "measureRect", {
	value: function () {
		var span = document.createElement("span");
		var textNode = document.createTextNode(this);
		span.appendChild(textNode);
		document.body.appendChild(span);
		if (document.createRange) {
			var range = document.createRange();
			range.selectNodeContents(textNode);
			if (range.getBoundingClientRect) {
				var rect = range.getBoundingClientRect();
				if (rect) {
					document.body.removeChild(span);
					return {
						width: rect.right - rect.left,
						height: rect.bottom - rect.top,
						rect: rect
					};
				}
			}
		}
		document.body.removeChild(span);
		return null;
	},
	enumerable: false
});

Object.defineProperty(Object.prototype, "ellipsis", {
	value: function (length) {
		var str = this.toString();
		if (str) {
			var len = str.measureRect().width;
			if (length > '...'.measureRect().width) {
				while (len > length) {
					str = str.slice(0, -1);
					len = (str + '...').measureRect().width
				}
			}
		}
		return str + '...';
	},
	enumerable: false
});

Object.defineProperty(Object.prototype, "unveil", {
	value: function (ko, cb) {
		var obj = this;
		var resp = {};
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				if (ko.isObservable(obj[key])) {
					resp[key] = ko.utils.unwrapObservable(obj[key]);
				} else {
					resp[key] = obj[key];
				}
			}
		}
		return cb ? cb(resp) : resp;
	},
	enumerable: false
});

Object.defineProperty(Array.prototype, "getUnique", {
	value: function (selector) {
		var u = {}, a = [];
		var transformed = selector ? this.map(selector) : this;
		for (var i = 0, l = transformed.length; i < l; ++i) {
			if (u.hasOwnProperty(transformed[i])) {
				continue;
			}
			a.push(this[i]);
			u[transformed[i]] = 1;
		}
		return a;
	},
	enumerable: true
});

Object.defineProperty(Array.prototype, "sortby", {
	value: function (selector) {
		this.sort(function (a, b) {
			return (selector(a) == selector(b)) ? 0 : ((selector(a) > selector(b)) ? 1 : -1);
		});
	},
	enumerable: true
});

Object.defineProperty(Number.prototype, "rangeOf", {
	value: function () {
		return Array.apply(null, { length: this }).map(Number.call, Number)
	},
	enumerable: true
});
Object.defineProperty(Object.prototype, "format", {
	value: function () {
		var monthNames = [
			"Ocak", "Şubat", "Mart",
			"Nisan", "Mayıs", "Haziran", "Temmuz",
			"Ağustos", "Eylül", "Ekim",
			"Kasım", "Aralık"
		];

		var date = new Date(this.toString());
		var day = date.getDate();
		var monthIndex = date.getMonth();
		var year = date.getFullYear();

		return day + ' ' + monthNames[monthIndex] + ' ' + year;
	},
	enumerable: false
});
saveFile = function (fileURL, fileName) {
	// for non-IE
	if (!window.ActiveXObject) {
		var save = document.createElement('a');
		save.href = fileURL;
		save.target = '_blank';
		save.download = fileName || 'unknown';

		var evt = new MouseEvent('click', {
			'view': window,
			'bubbles': true,
			'cancelable': false
		});
		save.dispatchEvent(evt);

		(window.URL || window.webkitURL).revokeObjectURL(save.href);
	}

	// for IE < 11
	else if (!!window.ActiveXObject && document.execCommand) {
		var _window = window.open(fileURL, '_blank');
		_window.document.close();
		_window.document.execCommand('SaveAs', true, fileName || fileURL)
		_window.close();
	}
}

persist = function (name, value, days) {
	var mydomstorage = window.localStorage || (window.globalStorage ? globalStorage[location.hostname] : null);
	if (mydomstorage) {
		mydomstorage[name] = value;
	}
	if (value) {
		var expires;
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		}
		else {
			expires = "";
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	} else {
		document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

}

extract = function (name) {
	var mydomstorage = window.localStorage || (window.globalStorage ? globalStorage[location.hostname] : null);
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(name + "=");
		if (c_start != -1) {
			c_start = c_start + name.length + 1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) {
				c_end = document.cookie.length;
			}
			return mydomstorage && mydomstorage[name] || unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return mydomstorage && mydomstorage[name];
}
