layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.TableHeader = "Öğrencinin Dersleri"
	app.locals.route = "/tedrisat/ogrenciler/:id"
	layoutvm app

