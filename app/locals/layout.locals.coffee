module.exports = (app) ->
	app.locals.AppName = "E-Mektep"
	app.locals.Title = "E-Mektep"
	app.locals.SchoolingMenuTitle = "Tedrisat"
	app.locals.MembershipMenuTitle = "Oturum"
	app.locals.RegisterTitle = "Sizi Kaydedelim"
	app.locals.RegisterSubtitle = "Bilgileriniz mi Yok?"
	app.locals.LoginTitle = "Sizi Böyle Alalım"
	app.locals.LoginSubtitle = "Zaten Kayıtlı mısınız?"
	app.locals.LogoutTitle = "Oturumu Kapat"
	app.locals.Developer = "lampiclobe" 
	app.locals.HomePageUrl = "/"
	app.locals.Slogan = "Öğrenmek Güzeldir..."
	app.locals.ExcelExportLabel = "Excel"
	app.locals.PdfExportLabel = "Pdf"
	app.locals.WordExportLabel = "Word"
	app.locals.schoolingMenu = [
		{ title: 'Ders Listesi', active: false, color: "blue", href: "/tedrisat/dersler" }
	]
	app.locals.membershipMenu = [
		{ title: 'Oturum Açın', className: 'sign in icon', href: "/uyelik/oturumac" }
		{ title: 'Yeni Kayıt', className: 'wizard icon', href: "/uyelik/yenikayit" }
		{ title: 'Şifremi Unuttum', className: 'idea icon', href: "/uyelik/sifremiunuttum" }
		{ title: 'Aktivasyon', className: 'settings icon', href: "/uyelik/aktivasyon" }
	]
	app.locals.mainPageMenu = [
		{ title: 'Akademik Takvim', active: false, color: "teal", href: "/akademik-takvim" }
		{ title: 'Duyurular', active: false, color: "red", href: "/duyurular" }
		{ title: 'Aktiviteler', active: false, color: "yellow", href: "/aktiviteler" }
		{ title: 'Haberler', active: false, color: "green", href: "/haberler" }
	]
	for m in app.locals.mainPageMenu
		m.active = m.href == "/#{app.locals.route}"
	for m in app.locals.schoolingMenu
		m.active = m.href == "/#{app.locals.route}"
