layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.Header = "Oturum Açın"
	app.locals.SubmitTitle = "Giriş"
	app.locals.LoginPlaceholder = "TCKN No"
	app.locals.PasswordPlaceholder = "Şifre"
	app.locals.route = "/uyelik/oturumac"
	layoutvm app

