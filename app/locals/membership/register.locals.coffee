layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.Header = "Kayıt Olun"
	app.locals.SubmitTitle = "Kaydolun"
	app.locals.ContactPlaceholder = "İletişim E-Postası"
	app.locals.PasswordPlaceholder = "Şifre"
	app.locals.route = "/uyelik/yenikayit"
	layoutvm app

