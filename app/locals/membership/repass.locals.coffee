layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.Header = "Şifremi Unuttum"
	app.locals.ContactPlaceholder = "İletişim E-Postası"
	app.locals.PasswordPlaceholder = "Şifre"
	app.locals.RememberedTitle = "Tıklayın"
	app.locals.RememberedSubtitle = "Şifrenizi Hatırladınız mı?"
	app.locals.route = "/uyelik/sifremiunuttum"
	layoutvm app

