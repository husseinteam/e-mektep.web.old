layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.color = "green"
	app.locals.route = "haberler"
	app.locals.Header = "Haberler"
	app.locals.ReadMore = "Devamını Okuyun"
	layoutvm app

