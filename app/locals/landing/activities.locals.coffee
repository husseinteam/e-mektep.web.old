layoutvm = require "../layout.locals.compiled"

module.exports = (app) ->
	app.locals.color = "yellow"
	app.locals.route = "aktiviteler"
	app.locals.Header = "Aktiviteler"
	app.locals.ReadMore = "Devamını Okuyun"
	layoutvm app