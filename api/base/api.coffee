
config = require "#{__dirname}/config.compiled"
Client = (require "websocket").client

module.exports = (parameters, onConnection, onMessage, failback) ->
	path = "#{config.ApiHost}/ws/#{parameters.route}"
	client = new Client
	client.on "connectFailed", (error)->
			failback? "connection failed: #{error}"
	client.on "connect", (connection) ->
		console.log "WebSocket Client Connected"
		connection.on "error", (error) ->
				failback? "connection error: #{error}"
		connection.on "close", () ->
				console.log "connection Closed"
		connection.on "message", (message) ->
			onMessage message
		onConnection connection
	
	client.connect path, ""
