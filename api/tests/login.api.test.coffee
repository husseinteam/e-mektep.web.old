
loginApi = require "../build/api/membership/login.api.compiled"
assert = require "assert" 

describe "loginApi", () ->
	it "should connect to websocket", (done) ->
		this.timeout(5000)
		cb = 
			onConnection: (conn) ->
				assert conn?, "conn not null" 
				conn.sendUTF JSON.stringify
					logon: "25871077074", 
					password: "1q2w3e4r5t"
			onMessage: (message) ->
				{done, token, uid} = message
				assert done "authentication failed"
				assert token? "token is #{token}"
				assert uid? "uid is #{uid}"
				done()
			failback: (error) ->
				console.log "error is #{error}"
				done()
		#loginApi cb
		done()
